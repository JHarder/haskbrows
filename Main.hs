import Graphics.UI.Gtk
import Graphics.UI.Gtk.WebKit.WebView

import System.Environment (getArgs)
import System.Directory (canonicalizePath, doesFileExist)
import Data.Maybe (listToMaybe)
import Data.List (isInfixOf, intercalate)
import Control.Monad (liftM)
import Control.Monad.IO.Class

-- TODO add updating of url bar when url is changed (sorta working)
-- TODO add tabs (sepparate thread per tab?) ??????? should wm handle this?
-- TODO insert/normal mode for efficency?

-- data Mode = Insert | Normal

main :: IO ()
main = do
  initGUI

  ----------------------------------------------------------
  -- create widgets
  window <- windowNew
  scrollWindow <- scrolledWindowNew Nothing Nothing
  webView <- webViewNew
  back <- buttonNew
  forward <- buttonNew
  quit <- buttonNew
  mainBox <- vBoxNew False 0
  buttonsBox <- hBoxNew False 0
  url <- entryNew
  goButton <- buttonNew
  sep <- vSeparatorNew

  windowSetTitle window "My Test Browser"

  ----------------------------------------------------------
  -- set attributes
  set back [ buttonLabel := "<--" ]
  set quit [ buttonLabel := "Quit" ]
  set goButton [ buttonLabel := "Go" ]
  set forward [ buttonLabel := "-->" ]
  set window [ containerBorderWidth := 0,
               containerChild := mainBox ]
  set scrollWindow [ containerChild := webView ]

  ----------------------------------------------------------
  -- packing the boxes
  boxPackStart buttonsBox back PackNatural 0
  boxPackStart buttonsBox forward PackNatural 0
  boxPackStart buttonsBox url PackGrow 0
  boxPackStart buttonsBox goButton PackNatural 0
  -- boxPackStart buttonsBox sep PackNatural 15
  boxPackStart buttonsBox sep PackNatural 10
  boxPackEnd buttonsBox quit PackNatural 0

  boxPackStart mainBox buttonsBox PackNatural 8
  boxPackStart mainBox scrollWindow PackGrow 0

  ----------------------------------------------------------
  -- add handles to buttons and window
  --

  {- Find a way to abstract away keypresses and
     keybinds.  Eventually the idea is to have
     customizeable keybinds which the user can
     define
  -}
  window `on` keyPressEvent $ tryEvent $ do
         [Control] <- eventModifier
         "q" <- eventKeyName
         liftIO mainQuit -- putStrLn "Ctrl-q pressed"
               
  -- this works but does not discriminate when entering text into an entry
  -- so typing Hello world will cause you to goButton back.. :(
  window `on` keyPressEvent $ tryEvent $ do
         [Shift] <- eventModifier
         "H" <- eventKeyName
         liftIO $ webViewGoBack webView
         
  window `on` keyPressEvent $ tryEvent $ do
         [Shift] <- eventModifier
         "L" <- eventKeyName
         liftIO $ webViewGoForward webView
                
  -- window `on` keyPressEvent $ tryEvent $ do
  --        -- [Control] <- eventModifier
  --        "j" <- eventKeyName
  --        liftIO $ putStrLn "I should be scrolling down" -- just need to figure out how...
                
  onClicked back    $ webViewGoBack webView >> updateUrlBar url webView
  onClicked forward $ webViewGoForward webView >> updateUrlBar url webView

  onClicked goButton        $ parseQuery url webView >> updateUrlBar url webView
  onEntryActivate url $ parseQuery url webView >> updateUrlBar url webView

  onClicked quit   mainQuit
  onDestroy window mainQuit

  ----------------------------------------------------------
  -- set homepage if one is given
  -- is Just the page given by command like arg or Nothing
  mHome <- liftM listToMaybe getArgs
  case mHome of
    Just arg -> do
               -- determine if the argument is a local file to display
               isLocalFile <- doesFileExist arg
               if isLocalFile then do
                                fullPath <- canonicalizePath arg
                                webViewLoadUri webView ("file://"++fullPath)
               -- or if it is a website to go to
               else webViewLoadUri webView $ normalizeUrl arg

    -- if no arguments were passed, go to default home page
    Nothing -> webViewLoadUri webView "http://www.duckduckgo.com"
  ----------------------------------------------------------
  -- display window and start loop
  updateUrlBar url webView
  widgetShowAll window
  mainGUI


parseQuery :: Entry -> WebView -> IO ()
parseQuery e w = do
  query <- entryGetText e
  if ' ' `elem` query
     then search  e w
     else goToUrl e w
  
updateUrlBar :: Entry -> WebView -> IO ()
updateUrlBar url webView = do
         maybePage <- webViewGetUri webView
         case maybePage of
           Just page -> entrySetText url page
           Nothing   -> return ()

search :: Entry -> WebView -> IO ()
search e w = go duckduckSearch e w
 where duckduckSearch s = "https://www.duckduckgo.com/?q=" ++ sanatise s
       sanatise = intercalate "+" . words

goToUrl :: Entry -> WebView -> IO ()
goToUrl url webView = go normalizeUrl url webView

normalizeUrl :: String -> String
normalizeUrl str = if any (`isInfixOf` str) ["http://","https://","file://"]
                   then str else "https://" ++ str

go :: (String -> String) -> Entry -> WebView -> IO ()
go fun e w = liftM fun (entryGetText e) >>= webViewLoadUri w
